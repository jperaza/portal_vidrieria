﻿using BaseProject.Models.CategoriaProducto;
using BaseProject.Repositories.CategoriaProductoManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class CategoriaProductoController : Controller
    {
        private readonly ICategoriaProductoManager _categoriaProductoManager;
        public CategoriaProductoController(ICategoriaProductoManager categoriaProductoManager)
        {
            _categoriaProductoManager = categoriaProductoManager;
        }

        // GET: CategoriaProducto
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerTabla()
        {
            var data = _categoriaProductoManager.ObtenerTabla();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearCategoria()
        {
            return PartialView(new CategoriaProductoViewModel
            {
                Activo = true
            });
        }

        [HttpPost]
        public ActionResult CrearCategoria(CategoriaProductoViewModel model)
        {
            var result = _categoriaProductoManager.CrearCategoria(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarCategoria(int idCategoria)
        {
            var result = _categoriaProductoManager.ObtenerCategoria(idCategoria);
            return PartialView(result);
        }

        [HttpPost]
        public ActionResult EditarCategoria(CategoriaProductoViewModel model)
        {
            var result = _categoriaProductoManager.EditarCategoria(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CambiarEstado(int Id)
        {
            var response = _categoriaProductoManager.CambiarEstado(Id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}