﻿using BaseProject.Models.Producto;
using BaseProject.Repositories.ProductoManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class ProductoController : Controller
    {
        private readonly IProductoManager _productoManager;

        public ProductoController(IProductoManager productoManager)
        {
            _productoManager = productoManager;
        }

        // GET: Producto
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerTabla()
        {
            var data = _productoManager.ObtenerTabla();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearProducto()
        {
            var model = _productoManager.ObtenerModeloCrear();
            return View(model);
        }

        [HttpPost]
        public ActionResult CrearProducto(ProductoViewModel model)
        {
            var result = _productoManager.CrearProducto(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubirAdjuntoProducto(int idProducto)
        {
            foreach (string fileName in Request.Files)
            {
                var file = Request.Files[fileName];
                if (file == null) continue;
                var tipoContenido = file.ContentType;
                var adjunto = new BinaryReader(file.InputStream).ReadBytes(Convert.ToInt32(file.InputStream.Length));


                var material = new AdjuntoProductoViewModel
                {
                    AdjuntoStream = adjunto,
                    nombre = file.FileName,
                    ProductoId = idProducto,
                    tipo = tipoContenido
                };

                var response = _productoManager.SubirAdjunto(material);


            }

            return null;
        }

        [HttpGet]
        public ActionResult EditarProducto(int idProducto)
        {
            var model = _productoManager.ObtenerProducto(idProducto);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditarProducto(ProductoViewModel model)
        {
            var response = _productoManager.EditarProducto(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerAdjunto(int idProducto)
        {
            var result = _productoManager.ObtenerAdjuntoProducto(idProducto);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DescargarAdjunto(int idProducto)
        {
            var result = _productoManager.ObtenerAdjuntoProducto(idProducto);
            return File(result.AdjuntoStream, result.tipo, result.nombre);
        }

        public JsonResult GetDetalleProducto(int idProducto)
        {
            var data = _productoManager.ObtenerDetalleProducto(idProducto);

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EliminarDetalle(int idDetalle)
        {
            var result = _productoManager.EliminarDetalle(idDetalle);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AgregarDetalle(ProductoDetalleViewModel detalle)
        {
            var result = _productoManager.AgregarDetalle(detalle);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CambiarEstado(int Id)
        {
            var response = _productoManager.CambiarEstado(Id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult VerProducto(int idProducto)
        {
            var model = _productoManager.VerProducto(idProducto);
            return PartialView(model);
        }

        public ActionResult ObtenerImagen(int idProducto)
        {
            var result = _productoManager.ObtenerAdjuntoProducto(idProducto);
            return File(result.AdjuntoStream, result.tipo);
        }

    }
}