﻿using BaseProject.Models.Kardex;
using BaseProject.Repositories.KardexManager;
using BaseProject.Repositories.SeguridadManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    [Authorize]
    public class KardexController : BaseController
    {
        private readonly IKardexManager _kardexManager;
        public KardexController(
            ISeguridadManager seguridadManager,
            IKardexManager kardexManager
            ): base(seguridadManager) {
            _kardexManager = kardexManager;
        }
        // GET: Kardex
        public ActionResult Index()
        {
            var model = ObtenerSucuralUsuario(User.Identity.Name);
            
            return View(model);
        }

        [HttpGet]
        public JsonResult ObtenerTabla()
        {
            var data = _kardexManager.ObtenerTabla(ObtenerSucuralUsuario(User.Identity.Name).SucursalId);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearTransaccion(int SucursalId)
        {
            var model = _kardexManager.ObtenerModeloCrear(SucursalId);
            return View(model);
        }

        [HttpPost]
        public ActionResult CrearTransaccion(KardexViewModel model)
        {
            var response = _kardexManager.CrearTransaccion(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VerificarCantidad(int MaterialId, int Cantidad)
        {
            var result = _kardexManager.VerificarCantidad(MaterialId, Cantidad);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult VerTransaccion(int idTransacion)
        {
            var model = _kardexManager.VerTransacion(idTransacion);
            model.Sucursal = ObtenerSucuralUsuario(User.Identity.Name).Sucursal;
            return PartialView(model);
        }

    }
}