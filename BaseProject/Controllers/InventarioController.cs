﻿using BaseProject.Models;
using BaseProject.Models.Inventario;
using BaseProject.Repositories.InventarioManager;
using BaseProject.Repositories.SeguridadManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class InventarioController : Controller
    {
        private readonly IInventaioManager _inventarioManager;
        private readonly ISeguridadManager _seguridadManeger;

        public InventarioController(
            IInventaioManager inventaioManager,
            ISeguridadManager seguridadManager)
        {
            _inventarioManager = inventaioManager;
            _seguridadManeger = seguridadManager;
        }

        // GET: Inventario
        public ActionResult Index()
        {
            var result = _seguridadManeger.BuscarUsuarioSucursal(User.Identity.Name);
            return View(result);
        }

        public JsonResult GetTabla()
        {
            var data = _inventarioManager.GetTabla(1);

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult AgregarMaterial(int idSucursal)
        {
            var lst = _inventarioManager.GetMaterialesPorAgregar(idSucursal);

            if(lst.Count > 0)
            {
                var model = new InventarioViewModel
                {
                    SucursalId = idSucursal,
                    lstMateriales = lst
                };

                return PartialView(model);
            }

            return Json(new ResponseMessage { Ok = false, Message = $"Ya se han agregado todos los materiales disponibles a esta sucursal" }, JsonRequestBehavior.AllowGet);
            

        }

        [HttpPost]
        public JsonResult AgregarMaterial(InventarioViewModel model)
        {
            var response = _inventarioManager.AgregarMaterial(model);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult VerMaterialInventario(int idInventario)
        {
            var model = _inventarioManager.VerMaterial(idInventario);
            model.Sucursal = _seguridadManeger.BuscarUsuarioSucursal(User.Identity.Name).Sucursal;

            return PartialView(model);
        }

    }
}