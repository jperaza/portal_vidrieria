﻿using BaseProject.Models.Materiales;
using BaseProject.Repositories.CategoriaManager;
using BaseProject.Repositories.MaterialesManager;
using BaseProject.Repositories.TipoMedidaManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class MaterialesController : Controller
    {
        private readonly IMaterialesManager _materialesManager;
        private readonly ITipoMedidaManager _tipoMedida;
        private readonly ICategoriaManager _categoriaManager;

        public MaterialesController(
            IMaterialesManager materialesManager,
            ITipoMedidaManager tipoMedida,
            ICategoriaManager categoriaManager)
        {
            _materialesManager = materialesManager;
            _tipoMedida = tipoMedida;
            _categoriaManager = categoriaManager;
        }
        // GET: Materiales
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetTabla()
        {
            var data = _materialesManager.GetTablaMateriales();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearMaterial()
        {
            var model = new MaterialesViewModel
            {
                lstTipoMedida = _tipoMedida.lstTipoMedidas(),
                Activo = true,
                lstCategoria = _categoriaManager.lstTipoCategoria()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult CrearMateriales(MaterialesViewModel model)
        {
            var response = _materialesManager.CrearMetarial(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarMaterial(int idMaterial)
        {
            var model = _materialesManager.ObtenerMaterial(idMaterial);
            model.lstTipoMedida = _tipoMedida.lstTipoMedidas();
            model.lstCategoria = _categoriaManager.lstTipoCategoria();
            return View(model);
        }

        [HttpPost]
        public ActionResult EditarMaterial(MaterialesViewModel model)
        {
            var response = _materialesManager.EditarMaterial(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CambiarEstado(int Id)
        {
            var response = _materialesManager.CambiarEstado(Id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubirAdjuntoMaterial(int idMaterial)
        {
            foreach(string fileName in Request.Files)
            {
                var file = Request.Files[fileName];
                if (file == null) continue;
                var tipoContenido = file.ContentType;
                var adjunto = new BinaryReader(file.InputStream).ReadBytes(Convert.ToInt32(file.InputStream.Length));


                var material = new AdjuntoMaterialViewModel { 
                    AdjuntoStream = adjunto,
                    nombre = file.FileName,
                    MaterialId = idMaterial,
                    tipo = tipoContenido
                };

                var response = _materialesManager.SubirAdjunto(material);

                
            }

            return null;
        }

        public ActionResult ObtenerAdjunto(int idMaterial)
        {
            var response = _materialesManager.ObtenerAdjunto(idMaterial);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DescargarAdjunto(int idAdjunto)
        {
            var response = _materialesManager.DescargarAdjunto(idAdjunto);


            return File(response.AdjuntoStream, response.tipo, response.nombre);
        }

        public ActionResult EliminarArchivo(int idAdjunto)
        {
            var response = _materialesManager.EliminarArchivo(idAdjunto);

            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}