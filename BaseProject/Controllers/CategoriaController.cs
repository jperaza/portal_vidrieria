﻿using BaseProject.Models.Categoria;
using BaseProject.Repositories.CategoriaManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class CategoriaController : Controller
    {
        private readonly ICategoriaManager _categoriaManager;

        public CategoriaController(ICategoriaManager categoriaManager)
        {
            _categoriaManager = categoriaManager;
        }

        // GET: Categoria
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerTabla()
        {
            var data = _categoriaManager.ObtenerListado();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearCategoria()
        {
            var model = new CategoriaViewModel {
                Activo = true
            };
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CrearCategoria(CategoriaViewModel model)
        {
            var response = _categoriaManager.Crear(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarCategoria(int idCategoria)
        {
            var model = _categoriaManager.ObtenerById(idCategoria);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EditarCategoria(CategoriaViewModel model)
        {
            var response = _categoriaManager.Editar(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CambiarEstado(int Id)
        {
            var response = _categoriaManager.CambiarEstado(Id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}