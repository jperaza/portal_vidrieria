﻿using BaseProject.Models.TipoMedida;
using BaseProject.Repositories.TipoMedidaManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class TipoMedidaController : Controller
    {
        private readonly ITipoMedidaManager _tipoMedida;
        
        public TipoMedidaController(ITipoMedidaManager tipoMedida)
        {
            _tipoMedida = tipoMedida;
        }
        // GET: TipoMedida
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetTabla()
        {
            var data = _tipoMedida.ObtenerListado();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearTipoMedida()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CrearTipoMedida(TipoMedidaViewModel model)
        {
            var response = _tipoMedida.CrearTipoMedida(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarTipoMedida(int idTipoMedida)
        {
            var model = _tipoMedida.ObtenerTipoMedida(idTipoMedida);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EditarTipoMedida(TipoMedidaViewModel model)
        {
            var response = _tipoMedida.EditarTipoMedida(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}