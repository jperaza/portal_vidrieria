﻿using BaseProject.Models.Base;
using BaseProject.Models.Inventario;
using BaseProject.Repositories.SeguridadManager;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class BaseController : Controller
    {

        public ISeguridadManager _seguridadManager;

        public BaseController(ISeguridadManager seguridadManager)
        {
            _seguridadManager = seguridadManager;
        }

        public SucursalUsuarioViewModel ObtenerSucuralUsuario(string Usuario)
        {
            return _seguridadManager.BuscarUsuarioSucursal(Usuario);
        }

    }
}