﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Helpers
{
    public static class EstadoTareaProyectoEnum
    {
        public static int Pendiente = 1;
        public static int EnProceso = 2;
        public static int Completado = 3;

    } 
}