﻿using AutoMapper.QueryableExtensions;
using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.CategoriaProducto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Repositories.CategoriaProductoManager
{
    public class CategoriaProductoManager : ICategoriaProductoManager
    {
        public List<CategoriaProductoViewModel> ObtenerTabla()
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.CategoriaProducto.ProjectTo<CategoriaProductoViewModel>().ToList();

                return model;
            }
        }

        public ResponseMessage CrearCategoria(CategoriaProductoViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var categoria = AutoMapper.Mapper.Map<CategoriaProducto>(model);

                    db.CategoriaProducto.Add(categoria);
                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"Categoria creada exitosamente!" };

                }
            }
            catch(Exception ex) {
                return new ResponseMessage { Ok = false, Message = $"Error {ex.Message}!" };
            }
        }

        public CategoriaProductoViewModel ObtenerCategoria(int idCategoria)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.CategoriaProducto.Where(x => x.Id == idCategoria).ProjectTo<CategoriaProductoViewModel>().FirstOrDefault();

                return model;
            }
        }

        public ResponseMessage EditarCategoria(CategoriaProductoViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var vidrieria = db.CategoriaProducto.Where(x => x.Id == model.Id).FirstOrDefault();
                    vidrieria.Descripcion = model.Descripcion;
                    vidrieria.Activo = model.Activo;

                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = "Categoria Editada exitosamente!" };
                }
            }
            catch(Exception ex) {
                return new ResponseMessage { Ok = false, Message = $"Error {ex.Message}" };
            }
        }

        public ResponseMessage CambiarEstado(int idCategoria)
        {
            using (var db = new VidrieriaEntities())
            {
                var categoria = db.CategoriaProducto.FirstOrDefault(x => x.Id == idCategoria);

                categoria.Activo = !categoria.Activo;
                var response = categoria.Activo ? "Habilitado" : "Inhabilitado";

                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = $"Categoria {response} exitosamente!" };
            }
        }

    }
}