﻿using BaseProject.Models;
using BaseProject.Models.CategoriaProducto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseProject.Repositories.CategoriaProductoManager
{
    public interface ICategoriaProductoManager
    {
        List<CategoriaProductoViewModel> ObtenerTabla();
        ResponseMessage CrearCategoria(CategoriaProductoViewModel model);
        CategoriaProductoViewModel ObtenerCategoria(int idCategoria);
        ResponseMessage EditarCategoria(CategoriaProductoViewModel model);
        ResponseMessage CambiarEstado(int idCategoria);
    }
}
