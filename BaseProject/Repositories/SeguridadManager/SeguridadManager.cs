﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.Inventario;
using BaseProject.Models.Seguridad;
using BaseProject.Utilidades;
using AutoMapper.QueryableExtensions;

namespace BaseProject.Repositories.SeguridadManager
{
    public class SeguridadManager : ISeguridadManager
    {

        #region Sucursales

        public ResponseMessage CrearSucursal(SucursalViewModel model)
        {
            using (var db = new seguridadEntities())
            {
                try
                {
                    var sucursal = new Sucursales
                    {
                        Direccion = model.direccion,
                        Sucursal = model.sucursal,
                        Telefono = model.telefono
                    };

                    db.Sucursales.Add(sucursal);
                    db.SaveChanges();

                    return new ResponseMessage
                    {
                        Ok = true,
                        Message = $"Sucursal {model.sucursal} creada exitosamente!"
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseMessage
                    {
                        Ok = false,
                        Message = $"Error al crear la sucursal!"
                    };
                }

            }
        }

        public List<SucursalViewModel> ObtenerSucursales()
        {
            using (var db = new seguridadEntities())
            {
                var model = db.Sucursales.Select(x => new SucursalViewModel
                {
                    id = x.id,
                    direccion = x.Direccion,
                    sucursal = x.Sucursal,
                    telefono = x.Telefono
                }).ToList();

                return model;
            }
        }

        public SucursalViewModel ObtenerSucursalPorId(int idSucursal)
        {
            using (var db = new seguridadEntities())
            {
                return db.Sucursales.Where(x => x.id == idSucursal).Select(x => new SucursalViewModel
                {
                    id = x.id,
                    sucursal = x.Sucursal,
                    direccion = x.Direccion,
                    telefono = x.Telefono
                }).FirstOrDefault();

            }
        }

        public ResponseMessage EditarSucursal(SucursalViewModel model)
        {
            using (var db = new seguridadEntities())
            {
                try
                {
                    var sucursal = db.Sucursales.FirstOrDefault(x => x.id == model.id);

                    sucursal.Sucursal = model.sucursal;
                    sucursal.Telefono = model.telefono;
                    sucursal.Direccion = model.direccion;

                    db.SaveChanges();

                    return new ResponseMessage
                    {
                        Ok = true,
                        Message = $"Sucursal editada exitosamente!"
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseMessage
                    {
                        Ok = false,
                        Message = "Error al editar la sucursal"
                    };
                }

            }
        }

        public SucursalUsuarioViewModel BuscarUsuarioSucursal(string usuario)
        {
            using(var db = new seguridadEntities())
            {
                var model = db.Usuario.Where(x => x.Usuario1 == usuario)
                            .ProjectTo<SucursalUsuarioViewModel>().FirstOrDefault();

                return model;
            }
        }

        #endregion


        #region Usuarios

        public List<UsuarioViewModel> ObtenerUsuarios()
        {
            using (var db = new seguridadEntities())
            {
                var model = (from x in db.aspnet_Membership
                             join a in db.aspnet_Users on x.UserId equals a.UserId
                             join b in db.Usuario on a.UserName equals b.Usuario1
                             select new UsuarioViewModel
                             {
                                 email = x.Email,
                                 UserId = x.UserId,
                                 usuario = a.UserName,
                                 cargo = b.Cargo,
                                 nombre = b.Nombre,
                                 idSucursal = b.idSucursal,
                                 sucursal = b.Sucursales.Sucursal
                             }).ToList();

                return model;
            }
        }

        public List<SelectListItem> Sucurales()
        {
            using(var db = new seguridadEntities())
            {
                var sucursales = db.Sucursales.Select(x => new SelectListItem
                {
                    Value = x.id.ToString(),
                    Text = x.Sucursal
                }).ToList();

                return sucursales;
            }
        }

        public ResponseMessage CrearUsuario(UsuarioViewModel model)
        {
            using(var db = new seguridadEntities())
            {
                var usuarioExiste = db.aspnet_Users.FirstOrDefault(x => x.LoweredUserName == model.usuario.ToLower());

                if (usuarioExiste != null)
                    return new ResponseMessage { Ok = false, Message = $"El usuario {model.usuario} ya existe!" };

                var newUser = Membership.CreateUser(model.usuario, model.contrasena, model.email);

                var userMembership = db.aspnet_Users.FirstOrDefault(x => x.LoweredUserName == model.usuario.ToLower());

                var user = new Usuario
                {
                    Cargo = model.cargo,
                    idSucursal = model.idSucursal,
                    Email = model.email,
                    Nombre = model.nombre,
                    Usuario1 = model.usuario,
                    UserId = userMembership.UserId
                };

                db.Usuario.Add(user);
                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = $"Usuario {model.usuario} creado Exitosamente!" };

            }
        }

        public UsuarioViewModel ObtenerUsuario(string usuario)
        {
            using(var db = new seguridadEntities())
            {
                var sucursales = Sucurales();
                var model = (from x in db.Usuario
                             join a in db.aspnet_Membership on x.UserId equals a.UserId
                             select new UsuarioViewModel
                             {
                                 email = x.Email,
                                 UserId = x.UserId,
                                 usuario = x.Usuario1,
                                 cargo = x.Cargo,
                                 nombre = x.Nombre,
                                 idSucursal = x.idSucursal,
                                 lstSucursal = db.Sucursales.Select(b => new SelectListItem
                                 {
                                     Text = b.Sucursal,
                                     Value = b.id.ToString()
                                 }).ToList()
                             }).FirstOrDefault(x => x.usuario == usuario);

                return model;
            }
        }

        public ResponseMessage EditarUsuario(UsuarioViewModel model)
        {
            try
            {
                using (var db = new seguridadEntities())
                {
                    var Usuario = db.Usuario.FirstOrDefault(x => x.UserId == model.UserId);
                    var membership = Membership.GetUser(model.UserId);

                    membership.Email = model.email;
                    
                    Membership.UpdateUser(membership);

                    Usuario.Nombre = model.nombre;
                    Usuario.Email = model.email;
                    Usuario.idSucursal = model.idSucursal;
                    Usuario.Cargo = model.cargo;

                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"Usuario Editado Exitosamente!" };

                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage { Ok = false, Message = "Error al tratar de actualizar el usuario!" };
            }
        }

        public ResponseMessage CambiarContrasena(UsuarioViewModel model)
        {
            try
            {
                MembershipUser membership = Membership.GetUser(model.usuario);
                membership.ChangePassword(membership.ResetPassword(), model.contrasena);

                return new ResponseMessage { Ok = true, Message = $"Contraseña cambiada exitosamente!" };


            }catch(Exception ex)
            {
                return new ResponseMessage { Ok = false, Message = $"Error al cambiar la contraseña" };
            }
        }

        #endregion

        #region Rol

        public Guid ObtenerApplicationId()
        {
            using(var db = new seguridadEntities())
            {
                return db.aspnet_Applications.FirstOrDefault().ApplicationId;
            }
        }

        public List<RolViewModel> ObtenerListaRoles()
        {
            using(var db = new seguridadEntities())
            {
                var model = db.aspnet_Roles.Select(x => new RolViewModel
                {
                    descripcion = x.Description,
                    idAplicaccion = x.ApplicationId,
                    RoleId = x.RoleId,
                    RoleName = x.RoleName
                }).ToList();

                return model;
            }
        }

        public RolViewModel ModelCrearRol()
        {
            return new RolViewModel
            {
                idAplicaccion = ObtenerApplicationId()
            };
        }

        public ResponseMessage CrearRol(RolViewModel model)
        {
            using (var db = new seguridadEntities())
            {
                var rolExiste = db.aspnet_Roles.FirstOrDefault(x => x.RoleName == model.RoleName);

                if (rolExiste != null)
                    return new ResponseMessage { Ok = false, Message = $"El Rol {model.RoleName} ya existe!" };
                    


                var objRole = new aspnet_Roles
                {
                    RoleName = model.RoleName,
                    LoweredRoleName = model.RoleName.ToLower(),
                    ApplicationId = model.idAplicaccion,
                    Description = model.descripcion
                };

                var sqlGuid = new SequentialGuid().GetGuid(DateTime.Now.Ticks + 1000);
                objRole.RoleId = sqlGuid;

                db.aspnet_Roles.Add(objRole);
                db.SaveChanges();
                return new ResponseMessage { Ok = true, Message = $"Se ha creado el rol {model.RoleName} exitosamente!" };
                
            }
        }

        public RolViewModel ObtenerRolPorNombre(string RoleName)
        {
            using (var db = new seguridadEntities())
            {
                var model = db.aspnet_Roles.Select(x => new RolViewModel
                {
                    descripcion = x.Description,
                    idAplicaccion = x.ApplicationId,
                    RoleId = x.RoleId,
                    RoleName = x.RoleName
                }).FirstOrDefault(x => x.RoleName == RoleName);


                return model;
            }
        }

        public ResponseMessage EditarRol(RolViewModel model)
        {
            using (var db = new seguridadEntities())
            {
                var PrivilegioExiste = db.aspnet_Roles.FirstOrDefault(x => x.RoleName == model.RoleName);



                if (PrivilegioExiste != null)
                    if (PrivilegioExiste.RoleId != model.RoleId)
                    {
                        return new ResponseMessage { Ok = false, Message = "Ya Existe el Rol!" };
                        
                    }


                var privilegio = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == model.RoleId);

                privilegio.RoleName = model.RoleName;
                privilegio.Description = model.descripcion;
                privilegio.LoweredRoleName = model.RoleName.ToLower();

                db.SaveChanges();
                return new ResponseMessage { Ok = true, Message = $"Rol Editado Exitosamente" };
                
            }
        }

        #endregion


        #region AsignarRoles

        public List<PermisosPorUsuarioViewModel> ObtenerRoles(Guid userId)
        {
            using(var db = new seguridadEntities())
            {
                var permisos = db.vw_aspnet_Roles.Select(x => new PermisosPorUsuarioViewModel
                {
                    DescripcionRol = x.Description,
                    RoleId = x.RoleId,
                    UserId = userId,
                    Rol = x.RoleName
                }).ToList();


                var PermisosXUsuario = db.vw_aspnet_UsersInRoles.Where(x => x.UserId == userId).ToList();

                if (PermisosXUsuario.Count == 0)
                    return permisos.Select(x => { x.Asignado = 2; return x; }).ToList();


                return permisos.Where(x => PermisosXUsuario.Any(c => c.RoleId == x.RoleId)).Select(x => { x.Asignado = 1; return x; }).ToList();

            }
        }

        public ResponseMessage AsignarRoles(Guid UserId, List<PermisosPorUsuarioViewModel> ListaDetalle)
        {
            try
            {
                using (var db = new seguridadEntities())
                {
                    ListaDetalle = ListaDetalle.Where(x => x.Asignado == 1).ToList();

                    var usuario = db.aspnet_Users.FirstOrDefault(x => x.UserId == UserId);

                    var rolesExistentes = db.aspnet_UsersInRoles_GetRolesForUser("vidrieria", usuario.UserName).ToList();

                    if (rolesExistentes.Count > 0)
                        db.EliminarRolesPorUsuario(UserId);

                    if(ListaDetalle.Count > 0)
                        ListaDetalle.ForEach(x => db.AgregarRolUsuario(UserId, x.RoleId));

                    
                    return new ResponseMessage { Ok = true, Message = $"Se han agregado {ListaDetalle.Count} permisos exitosamente!" };
                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = "Error al tratar de agregar los permisos" }; }
        }

        #endregion


    }
}