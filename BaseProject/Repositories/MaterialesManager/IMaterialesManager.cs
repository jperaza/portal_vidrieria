﻿using BaseProject.Models;
using BaseProject.Models.Materiales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseProject.Repositories.MaterialesManager
{
    public interface IMaterialesManager
    {
        List<MaterialesViewModel> GetTablaMateriales();
        ResponseMessage CrearMetarial(MaterialesViewModel model);
        MaterialesViewModel ObtenerMaterial(int idMaterial);
        ResponseMessage EditarMaterial(MaterialesViewModel model);
        ResponseMessage CambiarEstado(int idMaterial);
        ResponseMessage SubirAdjunto(AdjuntoMaterialViewModel model);
        AdjuntoMaterialViewModel ObtenerAdjunto(int idMaterial);
        AdjuntoMaterialViewModel DescargarAdjunto(int idAdjunto);
        ResponseMessage EliminarArchivo(int idAdjunto);

    }
}
