﻿using BaseProject.Models;
using BaseProject.Models.Kardex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseProject.Repositories.KardexManager
{
    public interface IKardexManager
    {
        List<KardexViewModel> ObtenerTabla(int idSucursal);
        KardexViewModel ObtenerModeloCrear(int idSucursal);
        ResponseMessage CrearTransaccion(KardexViewModel model);
        ResponseMessage VerificarCantidad(int MaterialId, int Cantidad);
        KardexViewModel VerTransacion(int idTransaccion);
    }
}
