﻿using BaseProject.DB;
using BaseProject.Models.Kardex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Web.Mvc;
using BaseProject.Models;

namespace BaseProject.Repositories.KardexManager
{
    public class KardexManager : IKardexManager
    {
        public List<KardexViewModel> ObtenerTabla(int idSucursal)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Kardex
                            .Where(x => x.SucursalId == idSucursal)
                            .ProjectTo<KardexViewModel>().ToList();
                return model;
            }
        }

        public KardexViewModel ObtenerModeloCrear(int idSucursal)
        {
            using(var db =new VidrieriaEntities())
            {
                var transaciones = db.Transaccion
                                    .ProjectTo<SelectListItem>().ToList();

                var materiales = db.Inventario
                                .Where(x => x.SucursalId == idSucursal)
                                .ProjectTo< SelectListItem>().ToList();

                return new KardexViewModel
                {
                    SucursalId = idSucursal,
                    lstTransacciones = transaciones,
                    lstProductos = materiales
                };

            }
        }

        public ResponseMessage CrearTransaccion(KardexViewModel model)
        {
            using(var db = new VidrieriaEntities())
            {

                var kardex = new Kardex
                {
                    Fecha = DateTime.Now,
                    Usuario = HttpContext.Current.User.Identity.Name,
                    Referencia = model.Referencia,
                    SucursalId = model.SucursalId,
                    TransaccionId = model.TransaccionId
                };

                db.Kardex.Add(kardex);
                
                foreach(var item in model.lstDetalle)
                {

                    var inventario = db.Inventario.FirstOrDefault(x => x.Id == item.MaterialId);


                    if(model.TransaccionId == 2)
                    {
                        db.SaveChanges();
                        var kardexDetalle = new KardexDetalle
                        {
                            InventarioId = item.MaterialId,
                            Cantidad = item.Cantidad,
                            KardexId = kardex.Id
                        };

                        db.KardexDetalle.Add(kardexDetalle);
                        inventario.Cantidad = inventario.Cantidad - item.Cantidad;
                        db.SaveChanges();
                    }
                    else
                    {
                        var kardexDetalle = new KardexDetalle
                        {
                            InventarioId = item.MaterialId,
                            Cantidad = item.Cantidad,
                            KardexId = kardex.Id
                        };

                        db.KardexDetalle.Add(kardexDetalle);
                        inventario.Cantidad = inventario.Cantidad + item.Cantidad;
                        db.SaveChanges();
                    }

                }

                return new ResponseMessage { Ok = true, Message = $"Transaccion creada exitosamente" };

            }
        }

        public ResponseMessage VerificarCantidad(int MaterialId, int Cantidad)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Inventario.FirstOrDefault(x => x.Id == MaterialId);

                if (model.Cantidad >= Cantidad)
                    return new ResponseMessage { Ok = true, Message = "Si puede Seguir" };

                return new ResponseMessage { Ok = false, Message = $"El Material {model.Materiales.Descripcion} solo cuenta con {model.Cantidad} en el inventario" };
            }
        }

        public KardexViewModel VerTransacion(int idTransaccion)
        {
            using(var db = new VidrieriaEntities())
            {
                var transaccion = db.Kardex.Where(x => x.Id == idTransaccion)
                                    .ProjectTo<KardexViewModel>().FirstOrDefault();

                var detalle = db.KardexDetalle.Where(x => x.KardexId == idTransaccion)
                                .ProjectTo<KardexDetalleViewModel>().ToList();

                transaccion.lstDetalle = detalle;

                return transaccion;
            }
        }

    }
}