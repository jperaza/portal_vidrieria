﻿using BaseProject.Models;
using BaseProject.Models.Producto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseProject.Repositories.ProductoManager
{
    public interface IProductoManager
    {
        List<ProductoViewModel> ObtenerTabla();
        ProductoViewModel ObtenerModeloCrear();
        ResponseMessage CrearProducto(ProductoViewModel model);
        ResponseMessage SubirAdjunto(AdjuntoProductoViewModel model);
        ProductoViewModel ObtenerProducto(int idProducto);
        AdjuntoProductoViewModel ObtenerAdjuntoProducto(int idProducto);
        List<ProductoDetalleViewModel> ObtenerDetalleProducto(int idProducto);
        ResponseMessage EliminarDetalle(int idDetalle);
        ResponseMessage AgregarDetalle(ProductoDetalleViewModel model);
        ResponseMessage EditarProducto(ProductoViewModel model);
        ResponseMessage CambiarEstado(int idProducto);
        ProductoViewModel VerProducto(int idProducto);

    }
}
