﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.Producto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Repositories.ProductoManager
{
    public class ProductoManager : IProductoManager
    {
        public List<ProductoViewModel> ObtenerTabla()
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Producto.ProjectTo<ProductoViewModel>().ToList();

                return model;
            }

        }

        public ProductoViewModel ObtenerModeloCrear()
        {
            using(var db = new VidrieriaEntities())
            {
                var lst = db.CategoriaProducto
                            .Where(x => x.Activo == true)
                            .ProjectTo<SelectListItem>().ToList();

                var lstprod = db.Materiales
                                .Where(x => x.Activo == true)
                                .ProjectTo<SelectListItem>().ToList();


                return new ProductoViewModel { Activo = true, lstCategoria = lst, lstProducto = lstprod };

            }
        }

        public ResponseMessage CrearProducto(ProductoViewModel model)
        {
            using(var db = new VidrieriaEntities())
            {
                var producto = new Producto
                {
                    Activo = model.Activo,
                    CategoriaProductoId = model.CategoriaProductoId,
                    Descripcion = model.Descripcion,
                    Nombre = model.Nombre
                };

                db.Producto.Add(producto);

                foreach(var item in model.lstDetalle)
                {
                    var detalle = new ProductoDetalle
                    {
                        MaterialId = item.MaterialId,
                        Formula = item.Formula,
                        ProductoId = producto.Id
                    };

                    db.ProductoDetalle.Add(detalle);
                    db.SaveChanges();

                }



                return new ResponseMessage { Ok = true, Message = producto.Id.ToString() };

            }
        }

        public ResponseMessage SubirAdjunto(AdjuntoProductoViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {

                    var existe = db.AdjuntoProducto.FirstOrDefault(x => x.ProductoId == model.ProductoId);

                    if(existe == null)
                    {
                        var adjunto = new AdjuntoProducto
                        {
                            AdjuntoStream = model.AdjuntoStream,
                            ProductoId = model.ProductoId,
                            Nombre = model.nombre,
                            Tipo = model.tipo
                        };

                        db.AdjuntoProducto.Add(adjunto);
                        db.SaveChanges();
                    }
                    else
                    {
                        existe.AdjuntoStream = model.AdjuntoStream;
                        existe.ProductoId = model.ProductoId;
                        existe.Nombre = model.nombre;
                        existe.Tipo = model.tipo;
                        db.SaveChanges();
                    }

                    return new ResponseMessage { Ok = true, Message = "Exito" };

                }
            }
            catch (Exception ex) { return new ResponseMessage { Ok = true, Message = "Error " }; };
        }

        public ProductoViewModel ObtenerProducto(int idProducto)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Producto.Where(x => x.Id == idProducto).ProjectTo<ProductoViewModel>().FirstOrDefault();
                var lst = db.CategoriaProducto
                            .Where(x => x.Activo == true)
                            .ProjectTo<SelectListItem>().ToList();

                var lstprod = db.Materiales
                                .Where(x => x.Activo == true)
                                .ProjectTo<SelectListItem>().ToList();

                model.lstCategoria = lst;
                model.lstProducto = lstprod;

                return model;
            }
        }

        public AdjuntoProductoViewModel ObtenerAdjuntoProducto(int idProducto)
        {
            using(var db = new VidrieriaEntities())
            {
                var adjunto = db.AdjuntoProducto
                    .Where(x => x.ProductoId == idProducto)
                    .FirstOrDefault();

                var model = Mapper.Map<AdjuntoProductoViewModel>(adjunto);

                return model;
            }
        }

        public List<ProductoDetalleViewModel> ObtenerDetalleProducto(int idProducto)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.ProductoDetalle
                                .Where(x => x.ProductoId == idProducto)
                                .ProjectTo<ProductoDetalleViewModel>().ToList();

                return model;
            }
        }

        public ResponseMessage EliminarDetalle(int idDetalle)
        {
            using(var db = new VidrieriaEntities())
            {
                var detalle = db.ProductoDetalle.FirstOrDefault(x => x.Id == idDetalle);
                db.ProductoDetalle.Remove(detalle);
                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = $"Material eliminado exitosamente!" };
            }
        }

        public ResponseMessage AgregarDetalle(ProductoDetalleViewModel model)
        {
            using(var db = new VidrieriaEntities())
            {
                var detalle = new ProductoDetalle
                {
                    MaterialId = model.MaterialId,
                    Formula = model.Formula,
                    ProductoId = model.Id
                };

                db.ProductoDetalle.Add(detalle);
                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = "Material agregado exitosamente!" };

            }
        }

        public ResponseMessage EditarProducto(ProductoViewModel model)
        {
            using(var db = new VidrieriaEntities())
            {
                var producto = db.Producto.FirstOrDefault(x => x.Id == model.Id);
                producto.Nombre = model.Nombre;
                producto.Descripcion = model.Descripcion;
                producto.CategoriaProductoId = model.CategoriaProductoId;

                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = "Producto editado exitosamente!" };


            }
        }

        public ResponseMessage CambiarEstado(int idProducto)
        {
            using (var db = new VidrieriaEntities())
            {
                var producto = db.Producto.FirstOrDefault(x => x.Id == idProducto);

                producto.Activo = !producto.Activo;
                var response = producto.Activo ? "Habilitado" : "Inhabilitado";

                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = $"Producto {response} exitosamente!" };
            }
        }

        public ProductoViewModel VerProducto(int idProducto)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Producto.Where(x => x.Id == idProducto).ProjectTo<ProductoViewModel>().FirstOrDefault();

                var detalle = db.ProductoDetalle
                                .Where(x => x.ProductoId == model.Id)
                                .ProjectTo<ProductoDetalleViewModel>()
                                .ToList();

                model.lstDetalle = detalle;

                return model;

            }
        }


    }
}