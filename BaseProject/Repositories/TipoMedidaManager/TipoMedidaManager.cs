﻿using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.TipoMedida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Repositories.TipoMedidaManager
{
    public class TipoMedidaManager : ITipoMedidaManager
    {
        public List<TipoMedidaViewModel> ObtenerListado()
        {
            using (var db = new VidrieriaEntities())
            {
                var model = db.TipoMedida.Select(x => new TipoMedidaViewModel
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Prefijo = x.Prefijo
                }).ToList();

                return model;
            }
        }

        public ResponseMessage CrearTipoMedida(TipoMedidaViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var tipoMedida = new TipoMedida
                    {
                        Nombre = model.Nombre,
                        Prefijo = model.Prefijo
                    };

                    db.TipoMedida.Add(tipoMedida);
                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"Se ha creado el tipo de medida {model.Nombre}" };

                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = $"Error al tratar de crear el tipo de medida" }; }
        }

        public TipoMedidaViewModel ObtenerTipoMedida(int idTipoMedida)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.TipoMedida.Where(x => x.Id == idTipoMedida).Select(x => new TipoMedidaViewModel
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Prefijo = x.Prefijo
                }).FirstOrDefault();

                return model;
            }
        }

        public ResponseMessage EditarTipoMedida(TipoMedidaViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var tipo = db.TipoMedida.FirstOrDefault(x => x.Id == model.Id);

                    tipo.Nombre = model.Nombre;
                    tipo.Prefijo = model.Prefijo;

                    db.SaveChanges();
                    return new ResponseMessage { Ok = true, Message = $"Tipo Medida No. {model.Id} se ha editado exitosamente!" };

                }

            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = $"Error al editar el tipo de medida" }; }
        }

        public List<SelectListItem> lstTipoMedidas()
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.TipoMedida.Select(x => new SelectListItem
                {
                    Text = x.Nombre,
                    Value = x.Id.ToString()
                }).ToList();

                return model;
            }
        }

    }
}