﻿using BaseProject.Models;
using BaseProject.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Repositories.InventarioManager
{
    public interface IInventaioManager
    {
        List<InventarioViewModel> GetTabla(int sucursalId);
        List<SelectListItem> GetMaterialesPorAgregar(int idSucursal);
        ResponseMessage AgregarMaterial(InventarioViewModel model);
        VerMaterialInventario VerMaterial(int idInventario);
    }
}