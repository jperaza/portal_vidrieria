﻿using AutoMapper.QueryableExtensions;
using BaseProject.DB;
using BaseProject.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.QueryableExtensions;
using System.Web.Mvc;
using BaseProject.Models;
using AutoMapper;

namespace BaseProject.Repositories.InventarioManager
{
    public class InventarioManager : IInventaioManager
    {
        public List<InventarioViewModel> GetTabla(int sucursalId)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Inventario
                            .Where(x => x.SucursalId == sucursalId)
                            .ProjectTo<InventarioViewModel>().ToList();

                return model;

            }
        }

        public List<SelectListItem> GetMaterialesPorAgregar(int idSucursal)
        {
            using(var db = new VidrieriaEntities())
            {
                var inventario = db.Inventario.Where(x => x.SucursalId == idSucursal).ToList();
                if (inventario.Count == 0)
                    return db.Materiales.AsEnumerable().Select(x => new SelectListItem { Text = $"{x.Codigo} - {x.Nombre}", Value = x.Id.ToString() }).ToList();


                var materiales = db.Materiales.ToList();

                var resultado = materiales.Where(x => !inventario.Any(f => f.MaterialId == x.Id)).ToList();

                return resultado.Select(x => new SelectListItem
                {
                    Text = $"{x.Codigo} - {x.Nombre}",
                    Value = x.Id.ToString()
                }).ToList();

            }
        }

        public ResponseMessage AgregarMaterial(InventarioViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var inventario = Mapper.Map<Inventario>(model);
                    db.Inventario.Add(inventario);
                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"Se ha agregado el material al inventario" };
                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = $"Error al tratar de agregar el material al inventario" }; };
        }

        public VerMaterialInventario VerMaterial(int idInventario)
        {
            using(var db = new VidrieriaEntities())
            {
                var inventario = db.Inventario.FirstOrDefault(x => x.Id == idInventario);

                var model = Mapper.Map<VerMaterialInventario>(inventario);

                return model;

            }
        }

    }
}