﻿using AutoMapper;
using BaseProject.DB;
using BaseProject.Models.Producto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public class ProductoMap : Profile
    {
        public ProductoMap()
        {
            CreateMap<Producto, ProductoViewModel>()
                .ForMember(d => d.Id, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.CategoriaProductoId, o => o.MapFrom(z => z.CategoriaProductoId))
                .ForMember(d => d.Categoria, o => o.MapFrom(z => z.CategoriaProducto.Descripcion))
                .ForMember(d => d.Descripcion, o => o.MapFrom(z => z.Descripcion))
                .ForMember(d => d.Activo, o => o.MapFrom(z => z.Activo))
                .ForMember(d => d.lstCategoria, o => o.Ignore());

            CreateMap<AdjuntoProducto, AdjuntoProductoViewModel>()
                .ForMember(d => d.idAdjunto, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.ProductoId, o => o.MapFrom(z => z.ProductoId))
                .ForMember(d => d.tipo, o => o.MapFrom(z => z.Tipo))
                .ForMember(d => d.nombre, o => o.MapFrom(z => z.Nombre))
                .ForMember(d => d.AdjuntoStream, o => o.MapFrom(z => z.AdjuntoStream))
                .ForMember(d => d.AdjuntoLength, o => o.MapFrom(z => Convert.ToBase64String(z.AdjuntoStream).Length.ToString()));

            CreateMap<ProductoDetalle, ProductoDetalleViewModel>()
                .ForMember(d => d.Id, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.MaterialId, o => o.MapFrom(z => z.MaterialId))
                .ForMember(d => d.Material, o => o.MapFrom(z => z.Materiales.Nombre))
                .ForMember(d => d.Formula, o => o.MapFrom(z => z.Formula));


        }
    }
}