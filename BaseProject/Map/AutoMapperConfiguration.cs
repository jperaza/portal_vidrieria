﻿using BaseProject.DB;
using BaseProject.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {

            AutoMapper.Mapper.Initialize(x =>
            {
                x.AddProfile<InventarioMap>();
                x.AddProfile<SucursalMap>();
                x.AddProfile<KardexMap>();
                x.AddProfile<TransaccionMap>();
                x.AddProfile<CategoriaProductoMap>();
                x.AddProfile<ProductoMap>();
                x.AddProfile<MaterialesMap>();
            });
        }
    }
}