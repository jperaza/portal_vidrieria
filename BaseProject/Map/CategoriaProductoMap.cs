﻿using AutoMapper;
using BaseProject.DB;
using BaseProject.Models.CategoriaProducto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Map
{
    public class CategoriaProductoMap : Profile
    {

        public CategoriaProductoMap()
        {
            CreateMap<CategoriaProducto, CategoriaProductoViewModel>()
                .ForMember(d => d.Id, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.Descripcion, o => o.MapFrom(z => z.Descripcion))
                .ForMember(d => d.Activo, o => o.MapFrom(z => z.Activo));

            CreateMap<CategoriaProductoViewModel, CategoriaProducto>()
                .ForMember(d => d.Id, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.Descripcion, o => o.MapFrom(z => z.Descripcion))
                .ForMember(d => d.Activo, o => o.MapFrom(z => z.Activo));

            CreateMap<CategoriaProducto, SelectListItem>()
                .ForMember(d => d.Value, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.Text, o => o.MapFrom(z => z.Descripcion));

        }
    }
}