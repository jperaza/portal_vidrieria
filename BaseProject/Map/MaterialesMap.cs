﻿using AutoMapper;
using BaseProject.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Map
{
    public class MaterialesMap: Profile 
    {
        public MaterialesMap()
        {
            CreateMap<Materiales, SelectListItem>()
                .ForMember(d => d.Value, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.Text, o => o.MapFrom(z => z.Nombre));
        }
    }
}