﻿using AutoMapper;
using BaseProject.DB;
using BaseProject.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Map
{
    public class InventarioMap : Profile
    {
        public InventarioMap()
        {
            CreateMap<Inventario, InventarioViewModel>()
                    .ForMember(dest => dest.SucursalId,
                                opt => opt.MapFrom(src => src.SucursalId))
                    .ForMember(dest => dest.MaterialId,
                                opt => opt.MapFrom(src => src.MaterialId))
                    .ForMember(dest => dest.Codigo,
                                opt => opt.MapFrom(src => src.Materiales.Codigo))
                    .ForMember(dest => dest.Id,
                                opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.Nombre,
                                opt => opt.MapFrom(src => src.Materiales.Nombre))
                    .ForMember(dest => dest.Observaciones,
                                opt => opt.MapFrom(src => src.Observaciones))
                    .ForMember(dest => dest.PrecioCompra,
                                opt => opt.MapFrom(src => src.PrecioCompra))
                    .ForMember(dest => dest.PrecioVenta,
                                opt => opt.MapFrom(src => src.PreacioVenta))
                    .ForMember(dest => dest.Cantidad,
                                opt => opt.MapFrom(src => src.Cantidad))
                    .ForMember(dest => dest.Proveedor,
                                opt => opt.MapFrom(src => src.Proveedor));

            CreateMap<Inventario, VerMaterialInventario>()
                    .ForMember(dest => dest.SucursalId,
                                opt => opt.MapFrom(src => src.SucursalId))
                    .ForMember(dest => dest.Nombre,
                                opt => opt.MapFrom(src => src.Materiales.Nombre))
                    .ForMember(dest => dest.PrecioVenta,
                                opt => opt.MapFrom(src => src.PreacioVenta))
                    .ForMember(dest => dest.base64Img,
                                opt => opt.MapFrom(src => string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(src.Materiales.AdjuntoMateriales.FirstOrDefault().AdjuntoStream ?? new byte[byte.MinValue]))));



            CreateMap<InventarioViewModel, Inventario>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.SucursalId, opt => opt.MapFrom(src => src.SucursalId))
                .ForMember(dest => dest.Cantidad, opt => opt.MapFrom(src => src.Cantidad))
                .ForMember(dest => dest.MaterialId, opt => opt.MapFrom(src => src.MaterialId))
                .ForMember(dest => dest.Observaciones, opt => opt.MapFrom(src => src.Observaciones))
                .ForMember(dest => dest.PreacioVenta, opt => opt.MapFrom(src => src.PrecioVenta))
                .ForMember(dest => dest.PrecioCompra, opt => opt.MapFrom(src => src.PrecioCompra))
                .ForMember(dest => dest.Proveedor, opt => opt.MapFrom(src => src.Proveedor))
                .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.Usuario, opt => opt.MapFrom(src => HttpContext.Current.User.Identity.Name));

            CreateMap<Inventario, SelectListItem>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Materiales.Nombre));



        }
    }
}