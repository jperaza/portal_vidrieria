﻿using AutoMapper;
using BaseProject.DB;
using BaseProject.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public class SucursalMap : Profile
    {
        public SucursalMap(){
            CreateMap<Usuario, SucursalUsuarioViewModel>()
                .ForMember(d => d.idUsuario, o => o.MapFrom(z => z.idSucursal))
                .ForMember(d => d.Usuario, o => o.MapFrom(z => z.Usuario1))
                .ForMember(d => d.SucursalId, o => o.MapFrom(z => z.idSucursal))
                .ForMember(d => d.Sucursal, o => o.MapFrom(z => z.Sucursales.Sucursal));

        }
            
    }
}