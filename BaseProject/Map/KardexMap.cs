﻿using AutoMapper;
using BaseProject.DB;
using BaseProject.Models.Kardex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public class KardexMap : Profile
    {
        public KardexMap()
        {
            CreateMap<Kardex, KardexViewModel>()
                .ForMember(d => d.Id, o => o.MapFrom(z => z.Id))
                .ForMember(d => d.Referencia, o => o.MapFrom(z => z.Referencia))
                .ForMember(d => d.TransaccionId, o => o.MapFrom(z => z.TransaccionId))
                .ForMember(d => d.Transaccion, o => o.MapFrom(z => z.Transaccion.Nombre))
                .ForMember(d => d.Fecha, o => o.MapFrom(z => z.Fecha))
                .ForMember(d => d.Usuario, o => o.MapFrom(z => z.Usuario))
                .ForMember(d => d.SucursalId, o => o.MapFrom(z => z.SucursalId))
                .ForMember(d => d.lstTransacciones, o => o.Ignore())
                .ForMember(d => d.Sucursal, o => o.Ignore());



            CreateMap<KardexDetalle, KardexDetalleViewModel>()
                .ForMember(d => d.Cantidad, o => o.MapFrom(z => z.Cantidad))
                .ForMember(d => d.MaterialId, o => o.MapFrom(z => z.InventarioId))
                .ForMember(d => d.Materiales, o => o.MapFrom(z => z.Inventario.Materiales.Nombre));

        }
    }
}