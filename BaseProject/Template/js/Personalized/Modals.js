﻿

function ShowModal(data) {
    $("#modalBody").html(data);
    $('#myModal').modal({ backdrop: 'static', keyboard: false });
    $("#myModal").modal("show");
}
