﻿
function MostrarModal(url,data) {

    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function () {
            LoadWaitNotification();
        },
        complete: function () {
            UnloadWaitNotification();
        },
        data: data,
        success: function (data) {
            ShowModal(data);
        }
    });

}


function AjaxFunctionCreateEdit(url, data, tabla) {

    $.ajax({
        url: url,
        type: "post",
        dataType: "JSON",
        data: data,
        beforeSend: function () {
            LoadWaitNotification();
        },
        complete: function () {
            UnloadWaitNotification();
        },
        success: function (response) {
            if (response.Ok) {
                $("#myModal").modal("hide");
                tabla.ajax.reload(null, false);
                AlertaAjax(response);
            }
            else {
                AlertaAjax(data);
            }
        }
    });

}
