﻿
function AlertaAjax(data) {
    console.log(data);
    swal("Advertencia", data.Message, data.Ok ? 'success' : 'error');
}

function AlertaError() {
    swal("Error de red", "", 'error');
}


function DeshabilitarRegistro(data, boton, url, Titulo, spanClass) {
    swal({
        html: "<hr>",
        title: '<span class="'+spanClass+'">'+Titulo+'</span>',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Si',
        confirmButtonColor: '#d33',
        cancelButtonText: 'No',
        allowOutsideClick: false
    }).then(function () {

        $.ajax({
            url: url,
            type: "post",
            data: data,
            success: function (data) {
                if (data.Ok) {
                    boton.ajax.reload();                    
                }
                AlertaAjax(data);
            },
            error: function (data) {

                swal('Error de red', '', 'error');

            },
        });
    });
}


function AlertaAjaxCallback(data, urlCallback) {
    console.log(data);
    swal("Advertencia", data.Message, data.Ok ? 'success' : 'error')
        .then((result) => {
            window.location.href = urlCallback;
        });
}

