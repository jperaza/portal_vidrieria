﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Kardex
{
    public class KardexViewModel
    {
        public int Id { get; set; }
        public string Referencia { get; set; }
        public int SucursalId { get; set; }
        public string Sucursal { get; set; }
        public int TransaccionId { get; set; }
        public string Transaccion { get; set; }
        public DateTime Fecha { get; set; }
        public string Usuario { get; set; }
        public List<KardexDetalleViewModel> lstDetalle { get; set; }
        public List<SelectListItem> lstTransacciones { get; set; }
        public List<SelectListItem> lstProductos { get; set; }
    }


    public class KardexDetalleViewModel
    {
        public int MaterialId { get; set; }
        public string Materiales { get; set; }
        public int Cantidad { get; set; }
    }

}