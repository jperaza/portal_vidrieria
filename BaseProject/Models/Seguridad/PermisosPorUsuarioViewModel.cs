﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Seguridad
{
    public class PermisosPorUsuarioViewModel
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string DescripcionRol { get; set; }
        public string Rol { get; set; }
        public int Asignado { get; set; }
    }
}