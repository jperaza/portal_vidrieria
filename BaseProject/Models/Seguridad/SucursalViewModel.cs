﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models
{
    public class SucursalViewModel
    {
        public int id { get; set; }
        public string sucursal { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
    }
}