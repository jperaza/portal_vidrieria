﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Seccion
{
    public class SeccionViewModel
    {
        public int Pk_IdSeccion { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        public string Descripcion { get; set; }

        public bool Publicado { get; set; }
        public bool Activo { get; set; }
        public Nullable<int> IdSeccionPadre { get; set; }

        //helpers
        public bool EsEditar { get; set; }
    }
}