﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Producto
{
    public class ProductoViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int CategoriaProductoId { get; set; }
        public string Categoria { get; set; }
        public bool Activo { get; set; }
        public List<SelectListItem> lstCategoria { get; set; }
        public List<SelectListItem> lstProducto { get; set; }
        public List<ProductoDetalleViewModel> lstDetalle { get; set; }
    }


    public class ProductoDetalleViewModel
    {
        public int Id { get; set; }
        public int MaterialId { get; set; }
        public string Material { get; set; }
        public string Formula { get; set; }
    }

    public class AdjuntoProductoViewModel
    {
        public int idAdjunto { get; set; }
        public string nombre { get; set; }
        public string tipo { get; set; }
        public int ProductoId { get; set; }
        public byte[] AdjuntoStream { get; set; }
        public string AdjuntoLength { get; set; }
    }

}