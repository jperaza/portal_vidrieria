﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Categoria
{
    public class CategoriaViewModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public string DescripcionActivo { 
            get
            {
                return ObtenerDescripcionActivo(Activo);
            } 
            set { }
        }


        private string ObtenerDescripcionActivo(bool activo)
        {
            return (activo) ? "Si" : "No";
        }

    }
}