﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Proyecto
{
    public class ListarFaseProyectoViewModel
    {
      
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public bool Activo { get; set; }
        public bool Estado { get; set; }
        public int IdProyecto { get; set; }

        public int PorcentajeTerminado { get; set; }

        public int TotalTareasPendientes { get; set; }
        public int TotalTareasEnProceso { get; set; }
        public int TotalTareasCompletadas { get; set; }



    }
}