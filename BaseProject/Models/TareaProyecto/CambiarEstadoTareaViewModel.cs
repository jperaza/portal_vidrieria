﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.TareaProyecto
{
    public class CambiarEstadoTareaViewModel
    {
        public int IdTarea { get; set; }
        public int IdEstado { get; set; }
    }
}