﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Usuario
{
    public class ListaDeUsuariosViewModel : CrearUsuarioViewModel
    {
        public bool EsUsuarioKid { get; set; }
        public string UrlAvatar { get; set; }

    }
}