﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.TipoMedida
{
    public class TipoMedidaViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Prefijo { get; set; }
    }
}