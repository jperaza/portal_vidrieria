﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Inventario
{
    public class SucursalUsuarioViewModel
    {
        public int idUsuario { get; set; }
        public string Usuario { get; set; }
        public int SucursalId { get; set; }
        public string Sucursal { get; set; }
    }
}