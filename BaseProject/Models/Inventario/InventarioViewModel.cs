﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Inventario
{
    public class InventarioViewModel
    {
        public int Id { get; set; }
        public int MaterialId { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public int SucursalId { get; set; }
        public string Sucursal { get; set; }
        public decimal PrecioCompra { get; set; }
        public decimal PrecioVenta { get; set; }
        public string Proveedor { get; set; }
        public string Observaciones { get; set; }
        public List<SelectListItem> lstMateriales { get; set; }
        public int Cantidad { get; set; }
        
    }

    public class VerMaterialInventario : InventarioViewModel
    {
        public string base64Img { get; set; }
    }

}