﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Materiales
{
    public class AdjuntoMaterialViewModel
    {
        public int idAdjunto { get; set; }
        public string nombre { get; set; }
        public string tipo { get; set; }
        public int MaterialId { get; set; }
        public byte[] AdjuntoStream { get; set; }
        public string AdjuntoLength { get; set; }
    }
}