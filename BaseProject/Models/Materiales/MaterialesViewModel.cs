﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Materiales
{
    public class MaterialesViewModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Marca { get; set; }
        public int TipoMedidaId { get; set; }
        public string TipoMedida { get; set; }
        public int CategoriaId { get; set; }
        public string Categoria { get; set; }
        public bool Activo { get; set; }
        public Nullable<decimal> Precio { get; set; }
        public string DescripcionActivo { get; set; }
        public List<SelectListItem> lstTipoMedida { get; set; }
        public List<SelectListItem> lstCategoria { get; set; }
    }
}