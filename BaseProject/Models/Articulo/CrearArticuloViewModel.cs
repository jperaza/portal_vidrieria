﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Articulo
{
    public class CrearArticuloViewModel
    {
        public int Pk_IdArticulo { get; set; }

        [Required]
        public string Titulo { get; set; }

        [Required]
        [AllowHtml]
        public string Contenido { get; set; }

        public HttpPostedFileBase Imagen { get; set; }

        public string UrlVideo { get; set; }

        public bool Publicado { get; set; }

        public bool Activo { get; set; }

        [Required]
        [DisplayName("Categoria")]
        public int Fk_IdSeccion { get; set; }

        public System.DateTime FechaCreacion { get; set; }

        public System.DateTime FechaPublicacion { get; set; }


        //flag
        public bool EsEditar { get; set; }
    }
}