﻿using System.Web;
using System.Web.Optimization;

namespace BaseProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Template/js/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
            "~/Template/js/bootstrap.min.js",
            "~/Template/js/nifty.min.js",
            "~/Template/js/pace.min.js",
            "~/Template/plugins/momentjs/moment.min.js",
            "~/Template/js/demo/bootstrap-material-datetimepicker.js",
            "~/Template/plugins/datatables/media/js/jquery.dataTables.js",
            "~/Template/plugins/datatables/media/js/dataTables.bootstrap.js",
            "~/Template/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js",
            "~/Template/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js",
            "~/Template/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js",
            "~/Template/plugins/select2/js/select2.min.js",
            "~/Template/plugins/bootstrap-sweetalert/sweetalert.min.js",
            "~/Template/plugins/chosen/chosen.jquery.min.js",
            "~/Template/plugins/summernote/summernote.js",
             "~/Template/plugins/summernote/lang/summernote-es-ES.js",
            "~/Template/plugins/dropzone/dropzone.min.js",
            "~/FontAwesome/js/all.min.js",
            "~/Template/plugins/easy-pie-chart/jquery.easypiechart.min.js",
            "~/Template/plugins/gauge-js/gauge.min.js",
            "~/Template/plugins/fullcalendar/lib/jquery-ui.custom.min.js",
            "~/Template/plugins/fullcalendar/fullcalendar.min.js",
            "~/Template/plugins/fullcalendar/lang/es.js",
            "~/Template/plugins/morris-js/morris.min.js",
            "~/Template/plugins/morris-js/raphael-js/raphael.min.js"
            

            // "~/Template/plugins/masonry/masonry.pkgd.min.js",
            //"~/Template/plugins/simplebar/simplebar.js"
            // "~/Template/plugins/Dual-Listbox-Transfer/js/jquery.transfer.js"


            ));

            bundles.Add(new ScriptBundle("~/bundles/personalized").Include(
            "~/Template/js/Personalized/accounting.min.js",
            "~/Template/js/Personalized/Alertas.js",
            "~/Template/js/Personalized/Conversion.js",
            "~/Template/js/Personalized/InputFormat.js",
            "~/Template/js/Personalized/Loading.js",
            "~/Template/js/Personalized/Modals.js",
             "~/Template/js/Personalized/Layout.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Template/css/bootstrap.min.css",
            "~/Template/css/nifty.min.css",
            "~/Template/css/pace.min.css",
             "~/Template/css/Layout.css",
            "~/Template/css/Modals.css",
            "~/Template/premium/icon-sets/icons/line-icons/premium-line-icons.css",
            "~/Template/premium/icon-sets/icons/solid-icons/premium-solid-icons.css",
            "~/FontAwesome/css/all.min.css",
            "~/Template/plugins/ionicons/css/ionicons.css",
            "~/Template/plugins/animate-css/animate.min.css",
            "~/Template/css/demo/bootstrap-material-datetimepicker.css",
            "~/Template/plugins/datatables/media/css/dataTables.bootstrap.css",
            "~/Template/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css",

            "~/Template/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css",
            "~/Template/plugins/select2/css/select2.min.css",
     
            "~/Template/plugins/bootstrap-sweetalert/sweetalert.css",
            "~/Template/plugins/summernote/summernote.min.css",
            "~/Template/plugins/dropzone/dropzone.min.css",
            "~/Template/css/NavTabsPersonalized.css",
            "~/Template/plugins/fullcalendar/fullcalendar.min.css",
            "~/Template/plugins/fullcalendar/nifty-skin/fullcalendar-nifty.min.css",
            "~/Template/plugins/morris-js/morris.min.css",
            "~/Template/plugins/chosen/chosen.min.css"
            //"~/Template/plugins/simplebar/simplebar.css"
            // "~/Template/plugins/Dual-Listbox-Transfer/css/jquery.transfer.css"
            ));

            BundleTable.EnableOptimizations = true;

        }
    }
}
